import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { MarkdefectComponent } from './markdefect/markdefect.component';
import { ViewdefectComponent } from './viewdefect/viewdefect.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TestComponent } from './test/test.component';
import { NgxBootstrapIconsModule } from 'ngx-bootstrap-icons';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    MarkdefectComponent,
    ViewdefectComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    TestComponent
  ],
  imports: [
    NgbModule,
    NgxBootstrapIconsModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path:'',component:HomeComponent},
      {path:'login',component:LoginComponent},
      {path:'registration',component:RegistrationComponent},
      {path:'viewdefect',component:ViewdefectComponent},
      {path:'markdefect',component:MarkdefectComponent},
      {path:'test',component:TestComponent},
      {path:'**',component:PageNotFoundComponent}
    ])],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
