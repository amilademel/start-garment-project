import { Component, OnInit } from '@angular/core';
import { GetCurrentYearService } from '../get-current-year.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
   currentYear!:Number;

  constructor(private dateService:GetCurrentYearService) { 
  }

  ngOnInit(): void {
    this.getCurretnDate();
  }

  getCurretnDate(): void{
  this.currentYear = this.dateService.getCurrentYear();
  console.log(this.currentYear)
  }

}
