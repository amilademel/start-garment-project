import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DefectServiceService {
 
    getDefects(){
      return [
        'Broken buttons',
        'Broken snaps',
        'Broken stitching',
        'Defective snaps',
        'Different shades within the same garment',
        'Dropped stitches',
        'Exposed notches',
        'Exposed raw edges',
        'Fabric defects',
        'Holes',
        'Inoperative zipper',
        'Loose / hanging sewing threads',
        'Misaligned buttons and holes',
        'Missing buttons',
        'Needle cuts / chews',
        'Open seams',
        'Pulled / loose yarn',
        'Stain',
        'Unfinished buttonhole',
        'Zipper too short'
      ];
   }
 
}
