import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from '../common/global-constants';
import { DecideEveOrMrgService } from '../decide-eve-or-mrg.service';
import { GetCurrentYearService } from '../get-current-year.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-markdefect',
  templateUrl: './markdefect.component.html',
  styleUrls: ['./markdefect.component.css']
})
export class MarkdefectComponent implements OnInit {

  info:string='';
  time!:Date;
  msg:string='';
  userName:string='';
  statusDefect:string='';
  statusRework:string='';
  statusGood:string='';
  numberOfRework:Number=125;
  numberOfGood:Number=8;
  numberOfDefect:Number=500;        
  
 
  constructor(
    private dateService:GetCurrentYearService,
    private decideServiceEveOrMrg: DecideEveOrMrgService,
    private userService:UserService) 
  {
    this.info=GlobalConstants.INFO;
    this.statusDefect=GlobalConstants.DEFECT;
    this.statusGood=GlobalConstants.GOOD;
    this.statusRework=GlobalConstants.REWORK;
   
   }
  
  ngOnInit(): void {   
    this.getCurrentDate();
    this.userName=this.userService.getUserDetaiils();
  }


  
  getCurrentDate() {
    setInterval(() => {
      this.time = this.dateService.getCurrentDate();
      this.msg=this.decideServiceEveOrMrg.decide();
    }, 1000); 
}
}
