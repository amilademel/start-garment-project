import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkdefectComponent } from './markdefect.component';

describe('MarkdefectComponent', () => {
  let component: MarkdefectComponent;
  let fixture: ComponentFixture<MarkdefectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkdefectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkdefectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
