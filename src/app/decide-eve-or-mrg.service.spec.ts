import { TestBed } from '@angular/core/testing';

import { DecideEveOrMrgService } from './decide-eve-or-mrg.service';

describe('DecideEveOrMrgService', () => {
  let service: DecideEveOrMrgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DecideEveOrMrgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
