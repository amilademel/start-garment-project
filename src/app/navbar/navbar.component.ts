import { Component, OnInit } from '@angular/core';
import{ GlobalConstants } from '../common/global-constants';
import { GetCurrentYearService } from '../get-current-year.service';
import { UserService } from '../user.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  appTitle:string;
  userName:string='';
  currentDate!:Date;
  constructor(private userService:UserService,private dateService:GetCurrentYearService) { 
    this.appTitle=GlobalConstants.siteTitle;
    this.userName=this.userService.getUserDetaiils();
    this.currentDate=dateService.getCurrentDate();
  }

  ngOnInit(): void {
  }

}
