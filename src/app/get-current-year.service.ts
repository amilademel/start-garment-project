import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetCurrentYearService {

  constructor() { }

  getCurrentYear(): Number {
    return new Date().getFullYear();
  }
  getCurrentHour(): Number {
    return new Date().getHours();
  }

  getCurrentDate(): Date{
    return  new Date(); 
  }
}
