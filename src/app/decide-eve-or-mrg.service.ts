import { Injectable } from '@angular/core';
import { GetCurrentYearService } from './get-current-year.service';

@Injectable({
  providedIn: 'root'
})
export class DecideEveOrMrgService {

  hours:Number=0;
  msg:string='';
  constructor(private dateService:GetCurrentYearService) { }

  
decide() {
  this.hours = this.dateService.getCurrentHour();
  if (this.hours < 10) {
      this.msg = 'Good Morning';
  } else if (this.hours < 16) {
      this.msg = 'Good Afternoon'; 
  } else if (this.hours < 19) {
      this.msg ='Good Evening';
  } else if (this.hours < 24) {
      this.msg = 'Good Night' ;
  } else if (this.hours < 6) {
      this.msg = 'Sleep lah'; 
  }
  return this.msg;
}

}
