import { Component, OnInit } from '@angular/core';
import { GlobalConstants } from '../common/global-constants';
import { GetCurrentYearService } from '../get-current-year.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefectServiceService } from '../defect-service.service';
import { DecideEveOrMrgService } from '../decide-eve-or-mrg.service';
import { UserService } from '../user.service';
import { cart2 } from 'ngx-bootstrap-icons';
import { platformBrowser } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  time!: Date;
  statusDefect!: string;
  statusGood!: string;
  statusRework!: string;
  msg: string = '';
  closeModal!: string;
  defectList: string[] = [];
  modalDefect: any;
  modalDefectMark: any;
  sampleImagURL!: string;
  userName: String = '';
  appInfo: string = '';

  constructor(
    private dateService: GetCurrentYearService,
    private modalService: NgbModal,
    private defectService: DefectServiceService,
    private decicdeService: DecideEveOrMrgService,
    private userService: UserService
  ) {
    this.statusDefect = GlobalConstants.DEFECT;
    this.statusRework = GlobalConstants.REWORK;
    this.statusGood = GlobalConstants.GOOD;
    this.sampleImagURL = GlobalConstants.IMGPATHSAMPLE;
    this.appInfo = GlobalConstants.INFO;
    console.log(this.sampleImagURL);
  }

  ngOnInit(): void {
    this.getCurrentDate();
    this.defectList = this.defectService.getDefects();
    this.userName = this.userService.getUserDetaiils();
  }


  getCurrentDate() {
    setInterval(() => {
      this.time = this.dateService.getCurrentDate();
      this.msg = this.decicdeService.decide();
    }, 1000);
  }

  onMarkDefect(content: any) {
    //button mark defect garments
    this.modalDefect = this.modalDefect = this.modalService.
      open(content, { ariaLabelledBy: 'modal-basic-defect', size: 'xl', centered: true }).result.then((res) => {
        this.closeModal = `Closed with: ${res}`;
      }, (res) => {
        this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      });
  }

  onMarkReWork() {
    //button mark rework garments
    alert('btn onMarkReWork');
  }


  onMarkPassed() {
    //button mark rework garments
    alert('btn onMarkPassed');
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  openModalPointGarment(content: any) {
    this.modalService.
      open(content, { ariaLabelledBy: 'modal-basic-defect', size: 'xl', centered: true }).result.then((res) => {
        this.closeModal = `Closed with: ${res}`;
      }, (res) => {
        this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      });
  }
}
