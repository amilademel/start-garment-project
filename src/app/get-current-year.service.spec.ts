import { TestBed } from '@angular/core/testing';

import { GetCurrentYearService } from './get-current-year.service';

describe('GetCurrentYearService', () => {
  let service: GetCurrentYearService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetCurrentYearService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
