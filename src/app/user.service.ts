import { Injectable } from '@angular/core';
import { GlobalConstants } from './common/global-constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userName:string='';
  constructor() {
    this.userName=GlobalConstants.SMAPLEUSERNAME;
   }

  getUserDetaiils(){
    return this.userName;
  }

}
