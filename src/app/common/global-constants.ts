export class GlobalConstants {
    public static  siteTitle: string = "Star Graments";
    public static DEFECT: string = "DEFECT";
    public static REWORK: string = "REWORK";
    public static GOOD: string = "GOOD";
    public static IMGPATHSAMPLE='/assets/images/shirt.jpg';
    public static SMAPLEUSERNAME='Amila De Mel';
    public static INFO:string=' Pecko Defect Tracking System is quick and efficient way to mark defect in finished garment,Help you to ease your headless defect detection process and focus more on your business.';
}