import { Component, OnInit } from '@angular/core';
import { alarm, alarmFill, alignBottom } from 'ngx-bootstrap-icons';
// Select some icons (use an object, not an array)
const icons = {
  alarm,
  alarmFill,
  alignBottom
};

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }

}
