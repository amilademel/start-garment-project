import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdefectComponent } from './viewdefect.component';

describe('ViewdefectComponent', () => {
  let component: ViewdefectComponent;
  let fixture: ComponentFixture<ViewdefectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewdefectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdefectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
